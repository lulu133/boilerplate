import logging
from rest_framework_swagger.views import get_swagger_view
from django.conf.urls import *
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework import routers
from .views import *
admin.autodiscover()

logger = logging.getLogger(__name__)
router = routers.DefaultRouter()
router.register(r'sample', SampleViewSet)
router.register(r'user', MeViewSet)

schema_view = get_swagger_view(title='API Docs')

urlpatterns = [
	url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    #url(r'^auth/', include('rest_framework_social_oauth2.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^djoser-auth/', include('djoser.urls')),
    url(r'^docs/$', schema_view),
    url(r'^plain/?$', plain, name='plain'),
]
