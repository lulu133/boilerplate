# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from djoser.signals import user_registered
from django.shortcuts import get_object_or_404, render
from django.views.generic.base import View
from django.contrib.auth.tokens import default_token_generator
from djoser.utils import decode_uid
from django.http import HttpResponse

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.filters import SearchFilter, OrderingFilter
from url_filter.integrations.drf import DjangoFilterBackend
from .serializers import *
from .permissions import *


def plain(request): 
    return HttpResponse("Hello world.")

class SampleViewSet(viewsets.ModelViewSet):
    '''
    * Sample Model Description: This is a description of the sample model.
    * CRUD on Sample model
    * C - CREATE - POST /sample/ : allowed as long as owner is the user creating the object 
    * R - READ - GET /sample/ (list) : user can see objects it owns
    * R - READ - GET /sample/[id]/ (detail) : user can see detail page of objects it owns
    * U - UPDATE - PATCH /sample/[id]/ : allowed for owner
    * D - DELETE - DELETE /sample/[id]/ : allowed for owner
    * Note in the case of a nested model A where a field f points to an instance of another model B, you can set f's value to an instance b of B by PATCHing or POSTing with f_id = [the id of b].
    '''
    queryset = Sample.objects.all()
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter,)  
    permission_classes = (SampleModelPermissions,)
    filter_fields = ('id', 'owner', 'stext', 'created', 'last_modified', )
    search_fields = ('stext',)
    serializer_class = SampleSerializer

    def get_queryset(self):
        logger.info('in SampleViewSet get_queryset method: ')
        if self.request.user.is_superuser:
            return Sample.objects.all()
        return Sample.objects.filter(owner=self.request.user) 


class MeViewSet(viewsets.ModelViewSet):
    '''
    * User Model Description: This is a description of the user model.
    * CRUD on User model
    * C - CREATE - POST /user/ : allowed as long as owner is the user creating the object 
    * R - READ - GET /user/ (list) : user can see objects it owns
    * R - READ - GET /user/[id]/ (detail) : user can see detail page of objects it owns
    * U - UPDATE - PATCH /user/[id]/ : allowed for owner
    * D - DELETE - DELETE /user/[id]/ : allowed for owner
    * Note in the case of a nested model A where a field f points to an instance of another model B, you can set f's value to an instance b of B by PATCHing or POSTing with f_id = [the id of b].
    '''
    queryset = User.objects.all()
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter,)  
    filter_fields = ('id', 'username', 'email', 'first_name', 'last_name')
    search_fields = ('first_name', 'last_name')
    serializer_class = UserSerializer

    def get_queryset(self):
        logger.info('in UserViewSet get_queryset method: ')
        if self.request.user.is_superuser:
            return User.objects.all()
        return User.objects.filter(id=self.request.user.id) 

