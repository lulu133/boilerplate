# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
from django.contrib.auth.models import User
from django.db import models

class Userstatus(models.Model): # no blank/null true needed here
    id = models.IntegerField(primary_key=True, default=0)
    owner = models.ForeignKey(User, related_name='owner_userstatus') # is admin owner of all userstatuses?
    user = models.OneToOneField(User, related_name='user_userstatus', on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    activation_token = models.CharField(max_length=255)
    last_modified = models.DateTimeField(default=datetime.datetime.utcnow, blank=True, null=True)
    created = models.DateTimeField(default=datetime.datetime.utcnow, blank=True, null=True)

    def __unicode__(self):
        return self.user.email

    def __str__(self):
        return self.user.email


class Sample(models.Model):
    created = models.DateTimeField(default=datetime.datetime.utcnow, blank=True, null=True)
    last_modified = models.DateTimeField(default=datetime.datetime.utcnow, blank=True, null=True)
    owner = models.ForeignKey(User, blank=True, null=True, related_name='owner_sample')
    stext = models.TextField(default='', blank=True, null=True)

    def __unicode__(self):
        return self.stext

    def __str__(self):
        return self.stext