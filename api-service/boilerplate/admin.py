# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *

class UserstatusAdmin(admin.ModelAdmin):
    list_display = ('owner', 'user', 'is_active')
    search_fields = ('first_name', 'last_name', 'email')

admin.site.register(Userstatus, UserstatusAdmin)

class SampleAdmin(admin.ModelAdmin):
    list_display = ('stext', 'owner', 'created', 'last_modified')
    search_fields = ('stext',)

admin.site.register(Sample, SampleAdmin)
