from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import *
from djoser import settings
from djoser import serializers as djoser_serializers

class UserRegistrationSerializer(djoser_serializers.UserRegistrationSerializer):
    class Meta(djoser_serializers.UserRegistrationSerializer.Meta):
        fields = tuple(User.REQUIRED_FIELDS) + (
            User.USERNAME_FIELD,
            User._meta.pk.name,
            'password',
            'first_name',
            'last_name',
        )

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups', 'first_name', 'date_joined', 'last_name')

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class UserstatusSerializer(serializers.ModelSerializer):
    owner = UserSerializer(read_only=True, required=False)
    owner_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), source='owner', write_only=True)
    user = UserSerializer(read_only=True, required=False)
    user_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), source='user', write_only=True)
    # the above is from http://stackoverflow.com/questions/29950956/drf-simple-foreign-key-assignment-with-nested-serializers
    class Meta:
        model = Userstatus
        fields = ('id', 'owner', 'user', 'is_active', 'activation_token', 'last_modified', 'created', 'owner_id', 'user_id')

class SampleSerializer(serializers.ModelSerializer):
    owner = UserSerializer(read_only=True, required=False)
    owner_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), source='owner', write_only=True)
    class Meta:
        model = Sample
        fields = ('id', 'owner', 'stext', 'last_modified', 'created', 'owner_id')
