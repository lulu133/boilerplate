import logging
from rest_framework import permissions

logger = logging.getLogger(__name__)

class SampleModelPermissions(permissions.BasePermission):

    def has_permission(self, request, view):
        logger.info('in SampleModelPermissions has_permission, req method: ' + str(request.method))
        if request.method in permissions.SAFE_METHODS:
            if request.user.id:
                logger.info('SampleModelPermissions: has_permission: GET LIST: listing samples for user: ' + str(request.user.id))
            else:
                logger.info('SampleModelPermissions: has_permission: GET LIST: looks like user is not defined')
                return False
            return True
        elif request.method == 'POST':
            suggested_owner = None
            data = None
            is_postman = False
            try: 
                data = dict(request.data.iterlists()) # postman
                is_postman = True
            except AttributeError:
                data = request.data # drf tests
            try:
                logger.info('SampleModelPermissions: has_permission: POST: request dict should have a suggested owner: ' + str(dict(data)))
                if is_postman: 
                    suggested_owner = int(data['owner_id'][0]) # postman
                else:
                    suggested_owner = data['owner_id']
            except (IndexError, KeyError):
                logger.error('SampleModelPermissions: has_permission: POST: request made without owner_id: ' + str(data))
                return False
            return request.user.id == suggested_owner
        elif request.method == 'PATCH':
            return True
        elif request.method == 'DELETE':
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):
        logger.info('in SampleModelPermissions has_object_permission, req method: ' + str(request.method))
        if request.method in permissions.SAFE_METHODS:
            logger.info('SampleModelPermissions: has_obj_permission: GET DETAIL: checking permission for sample detail: ' + str(obj.id))
            return request.user == obj.owner
        elif request.method == 'PATCH': 
            logger.info('SampleModelPermissions: has_obj_permission: PATCH DETAIL: checking permission for sample detail: ' + str(obj.id))
            return request.user == obj.owner
        elif request.method == 'DELETE':
            logger.info('SampleModelPermissions: has_obj_permission: DELETE DETAIL: checking permission for sample detail: ' + str(obj.id))
            return request.user == obj.owner
        else:
            return False

