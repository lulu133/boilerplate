import React from 'react';

const year = new Date().getFullYear();

const Footer = () => {
	return (
		<div className='Footer centered bt b--light-gray'>
			<p>©Lulu-Boilerplate, { year }</p>
		</div>
	);
}

export default Footer;