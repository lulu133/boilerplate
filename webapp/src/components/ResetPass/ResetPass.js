import React from 'react';

const ResetPass = ({ onRouteChange }) => {
	return (
		<div>
			<article className="br3 ba dark-gray b--black-10 mv4 w-100 w-50-m w-25-l mw6 shadow-5 center">
				<main className="pa4 black-80">
				  <div className="measure">
				    <fieldset id="reset_pass" className="ba b--transparent ph0 mh0">
				      <legend className="f2 fw6 ph0 mh0">Reset Password</legend>
				      <p>Submit your email address and we’ll send you a link to reset your password.</p>
				      <div className="mt3">
				        <label className="db fw6 lh-copy f6" htmlFor="email-address">Email</label>
				        <input className="pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100" type="email" name="email-address"  id="email-address"/>
				      </div>
				    </fieldset>
				    <div className="">
				      <input 
				      	onClick={() => onRouteChange('reset_thanks')}
					      className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib" 
					      type="submit" 
					      value="Send message"
					    />
				    </div>
				  </div>
				</main>
			</article>
		</div>
	);
}

export default ResetPass;