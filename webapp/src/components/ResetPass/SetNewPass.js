import React from 'react';

const SetNewPass = ({ onRouteChange }) => {
	return (
		<div>
			<article className="br3 ba dark-gray b--black-10 mv4 w-100 w-50-m w-25-l mw6 shadow-5 center">
				<main className="pa4 black-80">
				  <div className="measure">
				    <fieldset id="set_new_pass" className="ba b--transparent ph0 mh0">
				      <legend className="f2 fw6 ph0 mh0">Set your new password</legend>
				      <div className="mv3">
				        <label className="db fw6 lh-copy f6" htmlFor="password">Password</label>
				        <input className="b pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100" type="password" name="password"  id="password"/>
				      </div>
				      <div className="mv3">
				        <label className="db fw6 lh-copy f6" htmlFor="password">Confirm password</label>
				        <input className="b pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100" type="password" name="confirm_password"  id="confirm_password"/>
				      </div>
				    </fieldset>
				    <div className="">
				      <input 
				      	onClick={() => onRouteChange('signin')}
					      className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib" 
					      type="submit" 
					      value="Submit"
					    />
				    </div>
				  </div>
				</main>
			</article>
		</div>
	);
}

export default SetNewPass;