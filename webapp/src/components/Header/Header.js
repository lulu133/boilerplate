import React from 'react';
import Logo from './Logo';
import { Link } from 'react-router-dom';

const Navigation = ({onRouteChange, isSignedIn}) => {
	var menu = ''
	if (isSignedIn) {
		menu = <div class="flex-grow pa3 flex items-center">
						<p onClick={() => onRouteChange('signout')} className='f3 link dim black underline pa3 pointer'>Sign out</p>
					</div>
	} else {
		menu = <div class="flex-grow pa3 flex items-center">
						<p onClick={() => onRouteChange('signin')} className='f3 link dim black underline pa3 pointer'>Sign in</p>
						<p onClick={() => onRouteChange('register')} className='f3 link dim black underline pa3 pointer'>Register</p>
					</div>
	}
	return (
		<nav class="flex justify-between bb b--white-10">
			<Link to="/" class=" white-70 hover-white no-underline flex items-center pa3">
			  <Logo />
			</Link>
			{ menu }
		</nav>
	);
}

export default Navigation;
