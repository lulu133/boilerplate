import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import Particles from 'react-particles-js';
import Footer from '../components/Footer/Footer';
import HomePage from './HomePage/HomePage';
import Header from '../components/Header/Header';


const particlesOptions = {
  particles: {
    number: {
      value: 30,
      density: {
        enable: true,
        value_area: 800
      }
    }
  }
}
          
class App extends Component {
	constructor() {
		super();
		this.state = {
			route: 'signin',
			isSignedIn : false,
		}
	}

	onRouteChange = (route) => {
		if (route === 'home') {
			this.setState({ isSignedIn: true })
		}
		else {
			this.setState({ isSignedIn: false })
		}
		this.setState({ route: route })
	}

	render() {
		const { isSignedIn, route } = this.state;
		return (
			<Router>
				<div className='App'>
					<Helmet titleTemplate="Boilerplate" defaultTitle="Boilerplate">
		        <meta name="description" content="Boilerplate description"/>
		      </Helmet>
					<Particles className='particles' params={{particlesOptions}} />
					<Header onRouteChange={this.onRouteChange} isSignedIn={isSignedIn}/>
		      <Route exact path="/" render={(props) => (
		      	<HomePage {...props} route={route} onRouteChange={this.onRouteChange}/>
		      )}/>
		      <Footer />
		    </div>
		 	</Router>
		);
	}
}

export default App;
