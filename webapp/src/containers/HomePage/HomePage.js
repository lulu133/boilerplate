import React from 'react';
import Signin from '../../components/Signin/Signin';
import Register from '../../components/Register/Register';
import ResetPass from '../../components/ResetPass/ResetPass';
import SetNewPass from '../../components/ResetPass/SetNewPass';
import ok from './ok.png';


const HomePage = ({ onRouteChange, route }) => {
	if (route === 'signin' || route === 'signout') {
		return <Signin onRouteChange={onRouteChange}/>;
	}
	else if (route === 'register') {
		return <Register onRouteChange={onRouteChange}/>;
	}
	else if (route === 'reset_form') {
		return <ResetPass onRouteChange={onRouteChange}/>;
	}
	else if (route === 'reset_thanks') {
		return (
			<div>
				<article className="br3 ba dark-gray b--black-10 mv4 w-100 w-50-m w-25-l mw6 shadow-5 center">
					<img style={{paddingTop: '5px'}} src={ok} alt='logo' width='50' height='50'/>
					<p>The activation link was sent successfully. Please check your email.</p>
				</article>
			</div>
		);
	}
	else if (route === 'set_new_pass') {
		return <SetNewPass onRouteChange={onRouteChange}/>;
	}
	else {
		return (
			<div>
				<p>Hello</p>
			</div>
		);
	}
}

export default HomePage;